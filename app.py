from flask import Flask
import sys

app = Flask(__name__)

@app.route('/')
def show_python_version():
    response = f"Python version: {sys.version}"
    print(response)
    shutdown_server()  # Call function to shutdown the server
    return response

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

