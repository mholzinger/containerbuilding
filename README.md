# containerbuilding


This open source project is part of an article posted to **cipherpop.com** - [Breaking out of the traditional Docker model in Gitlab](https://cipherpop.com/breaking-out-of-docker.html)


# Project Components 
We cover a few basic components in the project to show two models of building containers using the Gitlab CI Gitlab Runner.

- Docker in Docker (dind driver)
- Kaniko executor

# Python Web Application
The web application for this project is written in Python using Flask. It listens on a thread for a REST GET request, reports the installed python version and summarily exits, causing the container to shut down.

# Dockerfile
The Dockerfile for this project mounts an /app/ direcotry for our python web application and exposes port 5000.

# Gitlab-ci.yml
Project continuous integration is written using two pipeline job definitions: `build-slow:` and `build-fast:`. Both pipeline jobs build the same container file and push to the gitlab registry.

# Container Registry
This project uses the built in gitlab artifact registry for publishing the Docke container to, and is overwritten each time the job pipeline steps are successfully completed.
